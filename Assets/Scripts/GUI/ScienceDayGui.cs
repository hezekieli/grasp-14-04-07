﻿using UnityEngine;
using System.Collections;

public class ScienceDayGui : MonoBehaviour {

    void OnGUI()
    {
		// Make a background box
		GUI.Box(new Rect(Screen.width/2 - 10, Screen.height/2, 100, 90), "Grasp"); 

		// Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
		if(GUI.Button(new Rect(Screen.width/2, Screen.height/2 + 30, 80, 20), "Tutorial")) {
			Application.LoadLevel("Tutorial");
		}

		// Make the second button.
		if(GUI.Button(new Rect(Screen.width/2 ,Screen.height/2 + 60, 80, 20), "Sandbox")) {
			Application.LoadLevel("ScienceDay");
		}
	}
}

