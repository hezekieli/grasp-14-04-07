﻿using UnityEngine;
using System.Collections;

public class CrowdControl : MonoBehaviour {

	public AudioClip superCheer;
	public AudioClip basicCheer;
	public bool cheerable;
	public float defaultVolume;

	// Use this for initialization
	void Start () {
		cheerable = false;
		audio.volume = defaultVolume;
	
	}
	
	// Update is called once per frame
	void Update () {

		if (cheerable == true) {
			audio.volume = 0.5f;
			audio.clip = superCheer;
			audio.Play();
			audio.loop = false;
			cheerable = false;
		}

		if (!audio.isPlaying) {
			audio.clip = basicCheer;
			audio.volume = defaultVolume;
			audio.loop = true;
			audio.Play();
		}
	
	}
}
