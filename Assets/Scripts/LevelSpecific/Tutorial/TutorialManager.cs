﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script controllors tutorial level and triggers different events in
/// the world in specific moments. 
/// </summary>
public class TutorialManager : MonoBehaviour
{

    public GameObject beginElevatorEndPos;
    public GameObject beginElevator;
    public GameObject[] handPoints;
    public GameObject rightHand;
    public GameObject leftHand;
    public GameObject oneHandedWeight1;
    public GameObject oneHandedWeight2;
    public GameObject WeightCheckPoint;
    public GameObject endingElevator;
    public GameObject endingElevatorPointB;
    public GameObject movingRobe;
    public GUIText guitext;
    public GameObject flames1;
    public GameObject flames2;
    public GameObject ground;
    public GameObject player;
    public GameObject endLevelTrigger;
    public GameObject curtainEndLevel;
    public GameObject music;
    public GameObject crowd;
    public GameObject spawnCheckPos;
    public GameObject playerSpawnPosition;
    public GameObject crank;
    public GameObject[] movingProps;
    public GameObject princessTower;
    public GameObject princessTowerEndPos;
    public GameObject princess;
    public GameObject princessSpawnPos;
    public Camera camera;
    public Transform cameraPosFar;

    float taskTimer;
    PlayerControls controlsScript;
    CranckPickUp cranckScript;
    TransformItem crankElevatorScript;
    int mission;

    int pointCalculator;
    float distance;

    bool rHandCloseEnough = false;
    bool lHandCloseEnough = false;
    bool pointIsVisible = false;

    Vector2 forceToWeight1 = new Vector2(2000, 4000);
    Vector2 forceToWeight2 = new Vector2(1000, 2000);
    bool giveForceToweight = true;

    bool spawnEnabled = false;
    bool princessEnabled = false;

    // Use this for initialization
    void Start()
    {
        mission = -1;
        pointCalculator = 0;
        oneHandedWeight1.renderer.enabled = false;
        oneHandedWeight2.renderer.enabled = false;
        controlsScript = player.GetComponent<PlayerControls>();
        crankElevatorScript = endingElevator.GetComponent<TransformItem>();
        cranckScript = crank.GetComponent<CranckPickUp>();
        endLevelTrigger.GetComponent<BoxCollider2D>().enabled = false;
        endLevelTrigger.GetComponent<EndLevel>().enabled = false;
        guitext.text = "Welcome to the circus!";
        foreach (GameObject point in handPoints)
        {
            point.renderer.enabled = false;
        }
    }

 
    /// <summary>
    /// Update is called once per frame. In tutorialmanagers update every specific 
    /// situation is in one case element. Tutorial progress goes to next case always 
    /// when player completes required task.
    /// </summary>
    void Update()
    {
        if (Input.GetKeyUp("1")) player.transform.position = playerSpawnPosition.transform.position;
        if (Input.GetKeyUp("l")) Application.LoadLevel("TutoRial"); //eli siis L nappi 
        if (Input.GetKeyUp(KeyCode.Escape)) Application.LoadLevel("ScienceDayGui"); //eli siis L nappi 

        if (spawnEnabled) CheckPlayerPosition(player, playerSpawnPosition);
        if (princessEnabled) CheckPlayerPosition(princess, princessSpawnPos);
        if (beginElevator.transform.position == beginElevatorEndPos.transform.position && mission == -1)
        {
            beginElevator.GetComponent<TransformItem>().enabled = false;
            mission = 1;
        }
        switch (mission)
        {
            case 1:
                MoveHands();
                break;

            case 2:
                guitext.text = "Walk around with LB and RB. Pick up the weight and hold it above your head.";
                WeightObservator(oneHandedWeight1, forceToWeight1);
                break;

            case 3:
                guitext.text = "Push both of the weights up in the air.";
                WeightObservator(oneHandedWeight2, forceToWeight2);
                break;

            case 4:
                if (WeightsInHands())
                {
                    if (oneHandedWeight1.transform.position.y > WeightCheckPoint.transform.position.y &&
                        oneHandedWeight2.transform.position.y > WeightCheckPoint.transform.position.y)
                    {
                        if (taskTimer == float.MaxValue) taskTimer = Time.time;
                        if (taskTimer + 1f < Time.time)
                        {
                            taskTimer = float.MaxValue;
                            mission = 5;
                            crowd.GetComponent<CrowdControl>().cheerable = true;
                        }
                    }
                    else taskTimer = float.MaxValue;
                }
                break;

            case 5:
                guitext.text = "Rotate the crank to move up!";
                camera.transform.parent = null;
                camera.transform.position = cameraPosFar.position;
                camera.orthographicSize = 16f;
                if (endingElevator.transform.position == endingElevatorPointB.transform.position)
                {
                    crankElevatorScript.enabled = false;
                    cranckScript.enabled = false; //ei lopeta pyörimistä vaikka nämä temput tekee
                    crank.rigidbody2D.isKinematic = true;
                    flames1.renderer.enabled = true;
                    flames2.renderer.enabled = true;
                    foreach (GameObject prop in movingProps)
                    {
                        prop.GetComponent<TransformItem>().enabled = true;
                    }
                    if (princessTower.transform.position == princessTowerEndPos.transform.position)
                    {
                        princess.rigidbody2D.isKinematic = false;
                        princessEnabled = true;
                        mission = 6;
                    }
                }
                break;

            case 6:
                spawnEnabled = true;
                guitext.text = "Hang on to the hand and save the princess from the fiery tower!";
                movingRobe.GetComponent<TransformItem>().enabled = true;
                MoveFlamesHorisontally();
                mission = 7;
                break;

            case 7:
                if (controlsScript.GetLeftHandObj() == null || controlsScript.GetRightHandObj() == null) break;
                if (controlsScript.GetLeftHandObj().name == "princess" ||
                   controlsScript.GetRightHandObj().name == "princess")
                {
                    endLevelTrigger.GetComponent<BoxCollider2D>().enabled = true;
                    endLevelTrigger.GetComponent<EndLevel>().enabled = true;
                    mission = 8;
                }
                break;

        }

    }

    /// <summary>
    /// Checks if o must been spawned and spawns player if must
    /// </summary>
    private void CheckPlayerPosition(GameObject o, GameObject spawnPos)
    {
        if (o.transform.position.y < spawnCheckPos.transform.position.y)
        {
            o.rigidbody2D.velocity = Vector2.zero;
            o.transform.position = spawnPos.transform.position;
        }
    }


    /// <summary>
    /// Checks if in both hands is weight
    /// </summary>
    /// <returns>true if is, else false</returns>
    private bool WeightsInHands()
    {
        if (controlsScript.GetRightHandObj() == oneHandedWeight1 && controlsScript.GetLeftHandObj() == oneHandedWeight2) return true;
        if (controlsScript.GetRightHandObj() == oneHandedWeight2 && controlsScript.GetLeftHandObj() == oneHandedWeight1) return true;
        return false;
    }


    /// <summary>
    /// Shoots the weights from behind left courtain if must. Observes that player
    /// keeps weight in air over two seconds. If keeps, mission is increased by one. 
    /// </summary>
    /// <param name="weight">weight</param>
    private void WeightObservator(GameObject weight, Vector2 force)
    {
        weight.renderer.enabled = true;
        if (giveForceToweight) 
        {
            weight.rigidbody2D.AddForceAtPosition(force, weight.transform.position); 
            giveForceToweight = false;
        }
        if ((controlsScript.GetLeftHandObj() == weight || controlsScript.GetRightHandObj()) == weight &&
            (weight.transform.position.y > WeightCheckPoint.transform.position.y))
        {
            if (taskTimer == float.MaxValue) taskTimer = Time.time;
            if (taskTimer + 2f < Time.time)
            {
                giveForceToweight = true;
                taskTimer = float.MaxValue;
                mission++;
            }
        }
        else taskTimer = float.MaxValue;
    }
    

    /// <summary>
    /// Changes flames to move horisontally
    /// </summary>
    void MoveFlamesHorisontally()
    {
        flames1.GetComponent<TransformItem>().speed = 0.06f;
        flames1.GetComponent<TransformItem>().oneWay = false;
        flames1.GetComponent<TransformItem>().move = true;
    }


    /// <summary>
    /// Player must move hands to specific points. In this case we have five
    /// points where player needs to move hands.
    /// </summary>
    void MoveHands()
    {
        switch (pointCalculator)
        {
            case 0:
                guitext.text = "Hold your hand in the circle.";
                if (!pointIsVisible) ObservePoint();
                break;

            case 1:
                guitext.text = "Hold your hand in the circle.";
                if (!pointIsVisible) ObservePoint();
                break;

            case 2:
                camera.transform.parent = player.transform;
                camera.orthographicSize = 10f;
                guitext.text = "Walk with LB or RB and hold your hand in the circles.";
                if (!pointIsVisible) ObservePoint();
                break;

            case 3:
                guitext.text = "Walk with LB or RB and hold your hand in the circles.";
                if (!pointIsVisible) ObservePoint();
                break;

            case 4:
                guitext.text = "Walk with LB or RB and hold your hand in the circles.";
                if (!pointIsVisible) ObservePoint();
                break;

            case 5:
                mission++;
                break;
        }

    }


    /// <summary>
    /// Makes point visible and chekcs if player keeps hand close enough the red circle.
    /// Player must also keep hand close enough the circle at least 0.5 seconds. 
    /// </summary>
    private void ObservePoint()
    {
        handPoints[pointCalculator].renderer.enabled = true;
        rHandCloseEnough = CloseEnough(handPoints[pointCalculator].transform.position, rightHand.transform.position, 0.3f);
        lHandCloseEnough = CloseEnough(handPoints[pointCalculator].transform.position, leftHand.transform.position, 0.3f);
        if (rHandCloseEnough || lHandCloseEnough)
        {
            if (taskTimer == float.MaxValue) taskTimer = Time.time;
            if (taskTimer + 0.5f < Time.time)
            {
                handPoints[pointCalculator].renderer.enabled = false;
                pointIsVisible = false;
                pointCalculator++;
                taskTimer = float.MaxValue;
            }
        }
        else taskTimer = float.MaxValue;
    }


    /// <summary>
    /// Checks if two vector arw close enough
    /// </summary>
    /// <param name="firstvector">vector one</param>
    /// <param name="secondvector">vector two</param>
    /// <param name="maxdistance">max distance between two vectors</param>
    /// <returns>true if vectors are close enough, otherwice false</returns>
    private bool CloseEnough(Vector3 firstvector, Vector3 secondvector, float maxdistance)
    {
        if (Vector2.Distance(firstvector, secondvector) < maxdistance) return true;
        return false;
    }


    /// <summary>
    /// Ends tutorial level. Ending curtain walls down and level 
    /// will be changed. 
    /// </summary>
    internal void endLevel()
    {
        if (controlsScript.GetLeftHandObj() == princess || controlsScript.GetRightHandObj() == princess)
        {
            music.GetComponent<musicControl>().ending = true;
            guitext.text = "Good job.";
            curtainEndLevel.GetComponent<TransformItem>().enabled = true;
        }
        else return;
    }
}


//TODO: - Tarkistus että kentän lopetustriggeriin tullaan neidon kanssa
//      - Kahen käen juttu
//      - Veivin toiminta hissin ja lavasteiden kaasuna
//      - ilotulitteet hienoimmiksi
//      - Äänet
//      - Taustan miettiminen vielä
//      - Pelaajalle vapaus kikkailla
//      - Valot
//      - Dialogi
//      - Spritejen valkoiset reunat?
//      - Hajotettava laatikko tai tiilikasa?
