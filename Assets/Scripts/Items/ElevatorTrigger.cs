﻿using UnityEngine;
using System.Collections;

public class ElevatorTrigger : MonoBehaviour {

    //public GameObject elevator;
    public GameObject [] elevators;
    bool activated;
    public TutorialManager tutoScript;

	// Use this for initialization
	void Start () {
        activated = false;	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (!activated)
        {
            tutoScript.ground.GetComponent<BoxCollider2D>().enabled = false;
            //tutoScript.spawnEnabled = true;
            foreach (GameObject elevator in elevators)
            {
                elevator.GetComponent<TransformItem>().enabled = true;
            }   
            renderer.material.color = Color.green;
            activated = true;
        }

        else
        {
            activated = false;
            renderer.material.color = Color.red;
        }
        
    }
}
