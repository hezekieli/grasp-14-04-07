﻿using UnityEngine;
using System.Collections;

public class BowPickUp : CustomPickUp
{
    public override void Start()
    {
        player = GameObject.Find("PlayerCharacter");
        handL = GameObject.Find("playerHandL");
        handR = GameObject.Find("playerHandR");
        controlsScript = player.GetComponent<PlayerControls>();
        bowPickup = true;
    }

    public override bool PickUp()
    {
        Physics2D.IgnoreLayerCollision(8, 9, true);
        rigidbody2D.gravityScale = 0;
        transform.position = latestHand.position;
        transform.right = latestHand.position - player.transform.position;
        return false;
    }
}