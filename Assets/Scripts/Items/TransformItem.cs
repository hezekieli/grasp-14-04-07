﻿using UnityEngine;
using System.Collections;

public class TransformItem : MonoBehaviour
{
    //TODO kränkkitarkkailija yms...
    public bool rotate;
    public bool move;
    public bool oneWay;
    public bool lerpMove;
    public float speed;

    public Transform movePointA;
    public Transform movePointB;
    public GameObject lerpObject; //for cranks and such

    bool goA = true;
    CranckPickUp script;
    Vector2 movePointAVect;
    // Use this for initialization
    void Start()
    {
        if (lerpObject)
        {
            script = lerpObject.GetComponent<CranckPickUp>();
            movePointAVect = new Vector2(transform.position.x, transform.position.y);
        }

        /*if (movePointA == null)
        {

            movePointA.position = transform.position;
        }
         * */
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (move) Move();
        if (rotate) Rotate();
        if (oneWay) MoveOneWay();
        if (lerpMove) LerpMove();
    }

    private void LerpMove()
    {
        //Debug.Log(movePointA.position);
        transform.position = Vector2.Lerp(movePointAVect, movePointB.position, script.GetProgress());
    }


    void Move()
    {
        if (goA) transform.position = Vector2.MoveTowards(transform.position, movePointA.position, speed);
        if (!goA) transform.position = Vector2.MoveTowards(transform.position, movePointB.position, speed);
        if (Vector2.Distance(transform.position, movePointA.position) < 0.01f) goA = false;
        if (Vector2.Distance(transform.position, movePointB.position) < 0.01f) goA = true;
    }

    void Rotate()
    {
        transform.Rotate(transform.forward, 1f);
    }

    /// <summary>
    /// Moves object from point A to point B
    /// </summary>
    void MoveOneWay()
    {
        transform.position = Vector2.MoveTowards(transform.position, movePointB.position, speed);
    }

}