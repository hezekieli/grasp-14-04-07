﻿using UnityEngine;
using System.Collections;

public class WeightDropManager : MonoBehaviour {

	public AudioClip drop;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D collision){
		if (rigidbody2D.velocity.magnitude > 0.1f) {
			audio.PlayOneShot (drop);
		}
		
	}
}
