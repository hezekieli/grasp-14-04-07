﻿using UnityEngine;
using System.Collections;

public class CranckPickUp : CustomPickUp
{
    private float velocity = 0.0f;
    private float rounds = 0f;
    public float maxRounds = 5f;
    public float minRounds = -3f;

    private Vector2 stickPosOldL;
    private Vector2 analogueStickPosL = new Vector2(0f, 0f);

    private Vector2 stickPosOldR;
    private Vector2 analogueStickPosR = new Vector2(0f, 0f);

    public override void Start()
    {
        player = GameObject.Find("PlayerCharacter");
        handL = GameObject.Find("playerHandL");
        handR = GameObject.Find("playerHandR");
        controlsScript = player.GetComponent<PlayerControls>();
        crankPickup = true;
    }

    public override bool PickUp()
    {
        //   if (controlsScript.GetLeftHandItemScript() != null)
        //       if (controlsScript.GetLeftHandItemScript().crankPickup)
        //       {
        //           Vector2 analogueDistL = new Vector2(Input.GetAxis("HorizontalL"), Input.GetAxis("VerticalL"));
        //           if (analogueDistL.magnitude > 0.8f)
        //           {
        //               transform.up = (transform.position + new Vector3(Input.GetAxis("HorizontalL"), Input.GetAxis("VerticalL"))) - transform.position;
        //               Debug.DrawLine(transform.position, transform.position + new Vector3(Input.GetAxis("HorizontalL"), Input.GetAxis("VerticalL")) - transform.position);
        //           }
        //       }
        //   if (controlsScript.GetRightHandItemScript() != null)
        //       if (controlsScript.GetRightHandItemScript().crankPickup)
        //       {
        //           Vector2 analogueVectorR = new Vector2(Input.GetAxis("HorizontalR"), Input.GetAxis("VerticalR"));
        //
        //           //float analogueAngle = Vector2.Angle(Vector2.up, -analogueVectorR);
        //           float analogueAngle = GetAngle(Vector2.up, analogueVectorR);
        //
        //           if (analogueVectorR.magnitude > 0.8f)
        //           {
        //               transform.localEulerAngles = new Vector3(0, 0, Mathf.SmoothDampAngle(transform.eulerAngles.z, analogueAngle, ref velocity, 0.3f));
        //
        //               Debug.DrawLine(transform.position, transform.position + new Vector3(Input.GetAxis("HorizontalR"), Input.GetAxis("VerticalR")));
        //               Debug.Log(transform.localEulerAngles.z);
        //           }
        //       }
        //
        //https://docs.unity3d.com/Documentation/ScriptReference/Mathf.SmoothDampAngle.html

        if (controlsScript.GetLeftHandItemScript() != null)
            if (controlsScript.GetLeftHandItemScript().crankPickup)
            {
                stickPosOldL = analogueStickPosL;
                analogueStickPosL = new Vector2(Input.GetAxis("HorizontalL"), Input.GetAxis("VerticalL"));
                if (analogueStickPosL.magnitude > 0.9f)
                {
                    if (analogueStickPosL.y > 0)
                    {
                        rounds += (analogueStickPosL.x - stickPosOldL.x) / 4;
                    }
                    if (analogueStickPosL.y < 0)
                    {
                        rounds -= (analogueStickPosL.x - stickPosOldL.x) / 4;
                    }
                    if (rounds > maxRounds) rounds = maxRounds;
                    if (rounds < minRounds) rounds = minRounds;
                    Debug.Log(rounds);
                    transform.eulerAngles = new Vector3(0, 0, rounds * -360f);
                }
            }

        if (controlsScript.GetRightHandItemScript() != null)
            if (controlsScript.GetRightHandItemScript().crankPickup)
            {
                stickPosOldR = analogueStickPosR;
                analogueStickPosR = new Vector2(Input.GetAxis("HorizontalR"), Input.GetAxis("VerticalR"));
                if (analogueStickPosR.magnitude > 0.9f)
                {
                    if (analogueStickPosR.y > 0)
                    {
                        rounds += (analogueStickPosR.x - stickPosOldR.x) / 4;
                    }
                    if (analogueStickPosR.y < 0)
                    {
                        rounds -= (analogueStickPosR.x - stickPosOldR.x) / 4;
                    }
                    if (rounds > maxRounds) rounds = maxRounds;
                    if (rounds < minRounds) rounds = minRounds;
                    //Debug.Log(rounds);
                    transform.eulerAngles = new Vector3(0, 0, rounds * -360f);
                }
            }
        return true;
    }

    public float GetRounds()
    {
        return rounds;
    }

    public float GetProgress()
    {
        if (rounds > 0) return rounds / maxRounds;
        if (rounds < 0) return rounds / -minRounds;
        return 0;
    }
}