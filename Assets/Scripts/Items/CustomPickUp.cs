﻿using UnityEngine;
using System.Collections;

public class CustomPickUp : MonoBehaviour
{
    //http://docs.unity3d.com/Documentation/Components/ExtendingTheEditor.html
    public bool standardOneHand;
    public bool standardTwoHand;
    public bool bowPickup;
    public bool arrowPickup;
    public bool crankPickup;

    protected Transform leftHand;
    protected Transform rightHand;
    protected Transform latestHand;
    public Transform arrowAttach;
    public Transform arrowAttachPos;
    public Transform arrow;

    protected GameObject player;
    protected GameObject handL;
    protected GameObject handR; 
    protected Collider2D handObjL;
    protected Collider2D handObjR;

    public PlayerControls controlsScript { get; set; }

    public virtual void Start()
    {
        player = GameObject.Find("PlayerCharacter");
        controlsScript = player.GetComponent<PlayerControls>();

        if (standardOneHand) StandardOneHandPickup();
        if (standardTwoHand) StandardTwoHandPickup();
        if (bowPickup) gameObject.AddComponent("BowPickUp");
        if (arrowPickup)
        {
            gameObject.AddComponent("ArrowPickUp");
            gameObject.GetComponent<ArrowPickUp>().arrowAttach = arrowAttach;
            gameObject.GetComponent<ArrowPickUp>().arrow = arrow;
            gameObject.GetComponent<ArrowPickUp>().arrowAttachPos = arrowAttachPos;
        }
        if (crankPickup) gameObject.AddComponent("CranckPickUp");
        Destroy(gameObject.GetComponent("CustomPickUp"));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns>Does need attach?</returns>
    public virtual bool PickUp()
    {
        bool needAttach = false;
       // if (standardOneHand) StandardOneHandPickup();
       // if (standardTwoHand) { StandardTwoHandPickup(); needAttach = true; };
       // if (bowPickup) BowPickup();
       // if (arrowPickup) ArrowPickup();
       // if (crank) { RotateCrank(); needAttach = true; };
        return needAttach;
    }

    public void GetHandsPos(Transform pos, string axis)
    {
        if (axis == "HorizontalL") leftHand = pos;
        if (axis == "HorizontalR") rightHand = pos;
        latestHand = pos;
    }

    protected void StandardOneHandPickup()
    {
        Physics2D.IgnoreLayerCollision(8, 9, true);
        rigidbody2D.gravityScale = 0;
        transform.position = latestHand.position;
    }

    void StandardTwoHandPickup()
    {
        Physics2D.IgnoreLayerCollision(8, 9, true);
    }
}