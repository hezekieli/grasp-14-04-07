﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour
{

    float walkSpeed = 3f;

    public LayerMask player;
    public LayerMask pickupable;
    public LayerMask attachable;
    public LayerMask ground;

    public bool attachedL { get; set; }
    public bool attachedR { get; set; }
    public Collider2D handObjL { get; set; }
    public Collider2D handObjR { get; set; }
    public bool enableWalk { get; set; }
    public bool enableStomp { get; set; }
    Transform groundCheckBody;

    Hand handL;
    Hand handR;

    SpriteRenderer spriteRenderer;
    public Sprite faceLeft;
    public Sprite faceRight;

    public bool firstStep; //if True, audio to be played is 1st step. if False, audio to be played is 2nd step
    public AudioClip footsteps1;
    public AudioClip footsteps2;

    void Start()
    {
        enableWalk = true;
        enableStomp = true;
        firstStep = true;
        // These in some global "init" script?
        for (int i = 0; i < 20; i++) if (i != 12) Physics2D.IgnoreLayerCollision(12, i, true);
        for (int i = 0; i < 20; i++) if (i != 13) Physics2D.IgnoreLayerCollision(13, i, true);// Ignores all collision for attach limit/middle layer
        Physics2D.IgnoreLayerCollision(12, 13, false);
        Physics2D.IgnoreLayerCollision(8, 9, true);

        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        groundCheckBody = GameObject.Find("GroundCheck").transform;

        handL = new Hand(gameObject, GameObject.Find("playerHandL"));
        handL.handRestPos = GameObject.Find("playerHandLRestPos").transform;
        handL.inputAxisX = "HorizontalL";
        handL.inputAxisY = "VerticalL";
        handL.pickUpKey = "TriggerL";
        handL.pickupable = pickupable;
        handL.attachable = attachable;
        handL.attachLimit = GameObject.Find("AttachLimitL").transform;
        handL.groundCheckBody = groundCheckBody;
        handL.player = player;
        handL.ground = ground;
        handL.body = gameObject;

        handR = new Hand(gameObject, GameObject.Find("playerHandR"));
        handR.handRestPos = GameObject.Find("playerHandRRestPos").transform;
        handR.inputAxisX = "HorizontalR";
        handR.inputAxisY = "VerticalR";
        handR.pickUpKey = "TriggerR";
        handR.pickupable = pickupable;
        handR.attachable = attachable;
        handR.attachLimit = GameObject.Find("AttachLimitR").transform;
        handR.groundCheckBody = groundCheckBody;
        handR.player = player;
        handR.ground = ground;
        handR.body = gameObject;
    }

    void FixedUpdate()
    {
        if (rigidbody2D.velocity.x < -1) spriteRenderer.sprite = faceLeft;
        if (rigidbody2D.velocity.x > 1) spriteRenderer.sprite = faceRight;

        bool isBodyOnGround = IsOnGround(groundCheckBody, 0.15f);
        if (Input.GetKey("joystick button 5") && isBodyOnGround && enableWalk)
        {
            Walk(true); //Right
            if (!audio.isPlaying)
            {
                if (firstStep == true)
                {
                    audio.clip = footsteps1;
                    audio.Play();
                    firstStep = false;
                }
                else
                {
                    audio.clip = footsteps2;
                    audio.Play();
                    firstStep = true;
                }
            }
        }
        if (Input.GetKey("joystick button 4") && isBodyOnGround && enableWalk)
        {
            Walk(false); //Left
            if (!audio.isPlaying)
            {
                if (firstStep == true)
                {
                    audio.clip = footsteps1;
                    audio.Play();
                    firstStep = false;
                }
                else
                {
                    audio.clip = footsteps2;
                    audio.Play();
                    firstStep = true;
                }
            }
        }

        handL.RunHand();
        attachedL = handL.attached;
        handR.RunHand();
        attachedR = handR.attached;
        handObjL = handL.handObj;
        handObjR = handR.handObj;
    }

    /// <summary>
    /// Makes the character walk left or right.
    /// </summary>
    /// <param name="isRight">False = left, true = right</param>
    void Walk(bool isRight)
    {
        if (isRight) rigidbody2D.velocity = new Vector2(walkSpeed, rigidbody2D.velocity.y);
        else rigidbody2D.velocity = new Vector2(-walkSpeed, rigidbody2D.velocity.y);
    }

    /// <summary>
    /// Checks if the given transform point is near enough to the ground
    /// </summary>
    /// <param name="checkable">Point which to check</param>
    /// <param name="circleSize">From how large of a radius to check</param>
    /// <returns>Is an object tagged "ground" inside the circle</returns>
    bool IsOnGround(Transform checkable, float circleSize)
    {
        if (Physics2D.OverlapCircle(checkable.transform.position, circleSize, ground) != null) return true;
        return false;
    }

    public GameObject GetLeftHandObj()
    {
        if (handObjL != null)
        {
            return handObjL.gameObject;
        }
        return null;
    }

    public GameObject GetRightHandObj()
    {
        if (handObjR != null)
        {
            return handObjR.gameObject;
        }
        return null;
    }

    public CustomPickUp GetLeftHandItemScript()
    {
        if (handL.custom != null)
        {
            return handL.custom;
        }
        return null;
    }

    public CustomPickUp GetRightHandItemScript()
    {
        if (handR.custom != null)
        {
            return handR.custom;
        }
        return null;
    }
}

/* TODO:
 * Bugs:
 * No player-pickupable collision after first pickup
 * Attach limit bugging
 * Min limit for groundstomp
 * Features:
 * Custom script support for pickupable items
 * Absolute position attach for large colliders
 * Mass / drag to hands?
 */

public class Hand
{
    public string inputAxisX;
    public string inputAxisY;
    public string pickUpKey;

    public LayerMask player;
    public LayerMask ground;
    public LayerMask pickupable;
    public LayerMask attachable;

    public GameObject body { get; set; }
    public GameObject hand { get; set; }
    public Transform handRestPos { get; set; }
    public Transform attachLimit { get; set; }
    public Transform groundCheckBody { get; set; }
    public PlayerControls controlsScript;
    public CustomPickUp custom { get; set; }

    bool handMovable;
    Vector2[] handPoses = new Vector2[10];
    int handPosesCounter = 0;
    public Collider2D handObj { get; set; }
    bool pickedUp;
    bool detached;
    Vector3 origAttachPos;
    PolygonCollider2D limiter;
    public bool attached { get; set; }
    int detachCooldown = 20;

    float handReach = 3f;
    float stompPower = 100f;
    float attachSwingPower = 60f;
    float throwPower = 300f;
    float snapLimit = 6.2f;

    /// <summary>
    /// Constructor
    /// </summary>
    public Hand(GameObject bodyo, GameObject hando)
    {
        body = bodyo;
        controlsScript = body.GetComponent<PlayerControls>();
        hand = hando;
    }

    /// <summary>
    /// Put this in Update(Fixed).
    /// </summary>
    public void RunHand()
    {
        attachLimit.transform.localScale = new Vector3(2.6f, 2.6f, 1f);
        snapLimit = 6.2f;

        hand.transform.up = handRestPos.transform.position - hand.transform.position;

        if (handMovable) MoveHand();
        handMovable = true;
        TickHandPosesCounter();
        RecordHandPose();
        if (IsOnGround(hand.transform, 0.65f) && !attached) HandStomp();
        if (handObj != null)
        {
            if (handObj.rigidbody2D != null) handObj.rigidbody2D.gravityScale = 1;
            body.rigidbody2D.gravityScale = 1;
        }
        attached = false;
        if (detached) TickDetachCooldown();
        if (Input.GetAxis(pickUpKey) > 0.1f && !detached)
        {
            handObj = TryPickUp();
            if (handObj != null && handObj.gameObject.layer == 9) PickUpObj();
            else
            {
                handObj = TryAttach();
                pickedUp = false;
                if (handObj != null)
                {
                    AttachToObj(false);
                    attached = true;
                    SwingBody();
                }
            }
        }
        else
        {
            if (detached) hand.renderer.material.color = Color.red;
            else hand.renderer.material.color = Color.white;
            if (!attached && handObj != null)
            {
                if (handObj.rigidbody2D != null) ThrowObject();
            }
            if (limiter != null) limiter.isTrigger = true;
            limiter = null;
            handObj = null;
            origAttachPos = Vector3.zero;
        }
        //     if (attachedSpring != null && !attached) attachedSpring.connectedBody = null; HOITAA ATTACHEDIN
        if (attached) handMovable = false;
    }

    /// <summary>
    /// Ticks the handPoses indexer forward
    /// </summary>
    void TickHandPosesCounter()
    {
        handPosesCounter++;
        if (handPosesCounter == handPoses.Length) handPosesCounter = 0;
    }

    /// <summary>
    /// Assigns the current hand location in the handPoses array
    /// </summary>
    void RecordHandPose()
    {
        handPoses[handPosesCounter] = hand.transform.localPosition;
    }

    /// <summary>
    /// Moves the hand according to the analogue stick
    /// </summary>
    void MoveHand()
    {
        hand.transform.position = new Vector2(handRestPos.position.x + Input.GetAxis(inputAxisX) * handReach, handRestPos.position.y + Input.GetAxis(inputAxisY) * handReach);
    }

    /// <summary>
    /// Checks if hand is on ground
    /// </summary>
    /// <param name="checkable">Point to check</param>
    /// <param name="circleSize">Size of the check circle</param>
    /// <returns>Is the point on ground</returns>
    bool IsOnGround(Transform checkable, float circleSize)
    {
        if (Physics2D.OverlapCircle(checkable.transform.position, circleSize, ground) != null) return true;
        return false;
    }

    /// <summary>
    /// Throws the character to a direction.
    /// </summary>
    void HandStomp()
    {
        if (controlsScript.enableStomp)
        {
            Vector2 oldPos = new Vector2();
            oldPos = GetOldPos();
            Vector2 stompForce = new Vector2(0, 0);
            stompForce = new Vector2(hand.transform.localPosition.x, hand.transform.localPosition.y) - oldPos;
            Debug.DrawLine(hand.transform.position, hand.transform.position + new Vector3(oldPos.x, oldPos.y, 0));
            body.rigidbody2D.AddForce(new Vector2(stompForce.x * -stompPower, stompForce.y * -stompPower));
        }
    }

    /// <summary>
    /// Gets the old position of the hand for calculations
    /// </summary>
    /// <returns>An old position from the handPoses array</returns>
    Vector2 GetOldPos()
    {
        if (handPosesCounter == handPoses.Length - 1) return handPoses[0];
        else return handPoses[handPosesCounter + 1];
    }

    /// <summary>
    /// Picks up a pickupable object
    /// </summary>
    void PickUpObj()
    {
        custom = handObj.GetComponent<CustomPickUp>();
        if (custom != null)
        {
            custom.GetHandsPos(hand.transform, inputAxisX);
            bool needAttach = custom.PickUp();
            if (needAttach)
            {
                AttachToObj(true);
                MoveObject();
            }
        }
        else
        {
            Physics2D.IgnoreLayerCollision(8, 9, true);
            if (handObj.rigidbody2D != null) handObj.rigidbody2D.gravityScale = 0;
            handObj.transform.position = hand.transform.position;
         //   AttachToObj(false);
        //    MoveObject();
        }
    }

    /// <summary>
    /// Tries to find a pickupable object. Returns null if none is found.
    /// </summary>
    /// <returns>Collider2D of the pickupable object. Null if nothing is found.</returns>
    Collider2D TryPickUp()
    {
        hand.renderer.material.color = Color.green;
        if (handObj == null)
        {
            Collider2D handObjNew = Physics2D.OverlapCircle(hand.transform.position, 0.7f, pickupable);
            return handObjNew;
        }
        else return handObj;
    }

    /// <summary>
    /// Tries to find an attachable object. Returns null if none is found.
    /// </summary>
    /// <returns>Collider2D of the pickuable object. Null if nothing is found.</returns>
    Collider2D TryAttach()
    {
        handObj = Physics2D.OverlapCircle(hand.transform.position, 0.7f, attachable);
        return handObj;
    }

    /// <summary>
    /// Attaches the body to the attachable object via spring joint.
    /// </summary>
    void AttachToObj(bool absolutePos)
    {
        hand.collider2D.isTrigger = true;
        if (!absolutePos)
        {
            hand.transform.position = handObj.transform.position;
            attachLimit.transform.position = handObj.transform.position;
        }
        else
        {
            if (origAttachPos == Vector3.zero) origAttachPos = hand.transform.position - handObj.transform.position;
            handMovable = false;
            attachLimit.transform.localScale = new Vector3(3f, 3f, 1f);
            snapLimit = 9.9f;
            hand.transform.position = handObj.transform.position + origAttachPos;
            attachLimit.transform.position = handObj.transform.position + origAttachPos;
        }

        limiter = attachLimit.GetComponent<PolygonCollider2D>();
        limiter.isTrigger = false;


        //  Debug.Log(Vector2.Distance(body.transform.position, handObj.transform.position));
        if (Vector2.Distance(body.transform.position, handObj.transform.position) > snapLimit) detached = true;
    }

    void MoveObject()
    {
       // if (IsOnGround(groundCheckBody, 0.15f))
       //     //        if (controlsScript.attachedL || controlsScript.attachedR)
       //     if (Vector2.Distance(body.transform.position, handObj.transform.position) < snapLimit - 0.5f)
       //         handObj.rigidbody2D.AddForce(new Vector2(Input.GetAxis(inputAxisX) * attachSwingPower, Input.GetAxis(inputAxisY) * attachSwingPower * 1.5f));
       // //   Debug.Log(Vector2.Distance(body.transform.position, handObj.transform.position));
        if (IsOnGround(groundCheckBody, 0.15f) || controlsScript.attachedL || controlsScript.attachedR
            && (Vector2.Distance(body.transform.position, handObj.transform.position) > snapLimit - 3f))
            handObj.rigidbody2D.AddForce(new Vector2(Input.GetAxis(inputAxisX) * attachSwingPower, Input.GetAxis(inputAxisY) * attachSwingPower * 1.5f));
    }

    void TickDetachCooldown()
    {
        detachCooldown--;
        if (detachCooldown <= 0)
        {
            detachCooldown = 20;
            detached = false;
        }
    }

    /// <summary>
    /// Moves the body around with the analogue axis.
    /// </summary>
    void SwingBody()
    {
        body.rigidbody2D.AddForce(new Vector2(Input.GetAxis(inputAxisX) * -attachSwingPower, Input.GetAxis(inputAxisY) * -attachSwingPower * 1.5f));
    }

    /// <summary>
    /// Pushes the object to a direction based on the current hand position and an old handpose
    /// </summary>
    void ThrowObject()
    {
        if (handObj == null) return;
        Vector2 oldPos = new Vector2();
        oldPos = GetOldPos();
        Vector2 throwForce = new Vector2(0, 0);
        throwForce = new Vector2(hand.transform.localPosition.x, hand.transform.localPosition.y) - oldPos;
        // Debug.Log("Throw!" + throwForce * throwPower);
        handObj.attachedRigidbody.velocity = Vector2.zero;
        handObj.attachedRigidbody.AddForce(new Vector2(throwForce.x * throwPower, throwForce.y * throwPower));

    }
}

// From Lauri with love