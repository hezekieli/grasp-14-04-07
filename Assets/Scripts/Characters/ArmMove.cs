﻿using UnityEngine;
using System.Collections;

public class ArmMove : MonoBehaviour {

	public GameObject hand;
	public GameObject arm;
	public GameObject foreArm;
	public float armLength = 2.1f;			// The whole arm length is 2x armLength
	public float shoulderX;
	public float shoulderY;
	// float elbowX;	// Same as forearm pivot, calculated in update()
	// float elbowY;	// --,,--


	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {

		// Selvitetään ekana kämmenen etäisyys olkapäästä (lepokohdasta, nollasta)
			// Jos käsi on täysin ojennettu, lepopaikassa tai diagonaalilla akselilla (x = y), niin käsivarren osat ovat täysimittaisia
				// Kyynärpää on aina kehosta pois päin, jos käsivarsi on taittunut

		float dx = hand.transform.localPosition.x;
		float dy = hand.transform.localPosition.y;

		// Laske etäisyys (ja kulma) kämmenestä olkapäähän (rest position)
		//float dHand = Mathf.Sqrt (Mathf.Pow (hand.transform.position.x, 2) + Mathf.Pow (hand.transform.position.y, 2));
		float dHand = Vector2.Distance (transform.position, hand.transform.position);
		//Debug.Log ("dHand = " + dHand);

		arm.transform.localScale = new Vector3(1, dHand / 3, 1);

		// Laske olkapäästä käteen ja olkapäästä kyynärpäähän välinen kulma
		float angleHand;
		if (hand.transform.localPosition.y == 0) angleHand = 90;
		else angleHand = Mathf.Atan (hand.transform.localPosition.x / hand.transform.localPosition.y)/Mathf.PI*180;

		//Debug.Log (hand.transform.localPosition.x + " . " + hand.transform.localPosition.y);
		//Debug.Log ("angleHand = "+angleHand);
		float angleArm = Mathf.Acos ((dHand/2)/armLength);
		//Debug.Log ("angleArm = " + angleArm);

		// Laske kyynärpään sijainti

		// Käännä kättä (arm ja forearm erikseen) 
		// When hand on the right
		if (dx > 0) 
		{
				if (dy < 0)	arm.transform.localEulerAngles = new Vector3 (0, 0, -angleHand);	// hand down
				else arm.transform.localEulerAngles = new Vector3 (0, 0, 180 - angleHand);	// hand up
		}
		// When hand on the left
		else 
		{
			if (dy < 0)	arm.transform.localEulerAngles = new Vector3 (0, 0, -angleHand); 	// hand down
			else arm.transform.localEulerAngles = new Vector3 (0, 0, 180 - angleHand);		// hand up
		}
		//arm.transform.localEulerAngles = new Vector3(0,0,Mathf.Abs(angleHand));
	}
}
